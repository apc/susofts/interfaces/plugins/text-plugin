#include <functional>
#include <map>
#include <string>

#include <Qsci/qscilexeravs.h>
#include <Qsci/qscilexerbash.h>
#include <Qsci/qscilexerbatch.h>
#include <Qsci/qscilexercmake.h>
#include <Qsci/qscilexercoffeescript.h>
#include <Qsci/qscilexercpp.h>
#include <Qsci/qscilexercsharp.h>
#include <Qsci/qscilexercss.h>
#include <Qsci/qscilexerd.h>
#include <Qsci/qscilexerdiff.h>
#include <Qsci/qscilexeredifact.h>
#include <Qsci/qscilexerfortran.h>
#include <Qsci/qscilexerfortran77.h>
#include <Qsci/qscilexerhtml.h>
#include <Qsci/qscilexeridl.h>
#include <Qsci/qscilexerjava.h>
#include <Qsci/qscilexerjavascript.h>
#include <Qsci/qscilexerjson.h>
#include <Qsci/qscilexerlua.h>
#include <Qsci/qscilexermakefile.h>
#include <Qsci/qscilexermarkdown.h>
#include <Qsci/qscilexermatlab.h>
#include <Qsci/qscilexeroctave.h>
#include <Qsci/qscilexerpascal.h>
#include <Qsci/qscilexerperl.h>
#include <Qsci/qscilexerpo.h>
#include <Qsci/qscilexerpostscript.h>
#include <Qsci/qscilexerpov.h>
#include <Qsci/qscilexerproperties.h>
#include <Qsci/qscilexerpython.h>
#include <Qsci/qscilexerruby.h>
#include <Qsci/qscilexerspice.h>
#include <Qsci/qscilexersql.h>
#include <Qsci/qscilexertcl.h>
#include <Qsci/qscilexertex.h>
#include <Qsci/qscilexerverilog.h>
#include <Qsci/qscilexervhdl.h>
#include <Qsci/qscilexerxml.h>
#include <Qsci/qscilexeryaml.h>

/* clang-format off */

#define _ADDLEXER(name, classname) {(name), [](QWidget *parent) -> QsciLexer * { return new classname(parent); }}

/** Map with all the default lexers provided by Qscintilla. */
const inline std::map<std::string, std::function<QsciLexer *(QWidget *)>> LEXERMAP = {
	_ADDLEXER("AVS", QsciLexerAVS),
	_ADDLEXER("Bash", QsciLexerBash),
	_ADDLEXER("Batch", QsciLexerBatch),
	_ADDLEXER("CMake", QsciLexerCMake),
	_ADDLEXER("CoffeeScript", QsciLexerCoffeeScript),
	_ADDLEXER("C++", QsciLexerCPP),
	_ADDLEXER("C#", QsciLexerCSharp),
	_ADDLEXER("CSS", QsciLexerCSS),
	_ADDLEXER("D", QsciLexerD),
	_ADDLEXER("Diff", QsciLexerDiff),
	_ADDLEXER("EDIFACT", QsciLexerEDIFACT),
	_ADDLEXER("Fortran", QsciLexerFortran),
	_ADDLEXER("Fortran77", QsciLexerFortran77),
	_ADDLEXER("HTML", QsciLexerHTML),
	_ADDLEXER("IDL", QsciLexerIDL),
	_ADDLEXER("Java", QsciLexerJava),
	_ADDLEXER("JavaScript", QsciLexerJavaScript),
	_ADDLEXER("JSON", QsciLexerJSON),
	_ADDLEXER("Lua", QsciLexerLua),
	_ADDLEXER("Makefile", QsciLexerMakefile),
	_ADDLEXER("Markdown", QsciLexerMarkdown),
	_ADDLEXER("Matlab", QsciLexerMatlab),
	_ADDLEXER("Octave", QsciLexerOctave),
	_ADDLEXER("Pascal", QsciLexerPascal),
	_ADDLEXER("Perl", QsciLexerPerl),
	_ADDLEXER("PO", QsciLexerPO),
	_ADDLEXER("PostScript", QsciLexerPostScript),
	_ADDLEXER("POV", QsciLexerPOV),
	_ADDLEXER("Properties", QsciLexerProperties),
	_ADDLEXER("Python", QsciLexerPython),
	_ADDLEXER("Ruby", QsciLexerRuby),
	_ADDLEXER("Spice", QsciLexerSpice),
	_ADDLEXER("SQL", QsciLexerSQL),
	_ADDLEXER("TCL", QsciLexerTCL),
	_ADDLEXER("TeX", QsciLexerTeX),
	_ADDLEXER("Verilog", QsciLexerVerilog),
	_ADDLEXER("VHDL", QsciLexerVHDL),
	_ADDLEXER("XML", QsciLexerXML),
	_ADDLEXER("YAML", QsciLexerYAML)
};

/* clang-format on */
