/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef TEXTPLUGIN_HPP
#define TEXTPLUGIN_HPP

#include <interface/SPluginInterface.hpp>

/**
 * Plugin interface for various text files.
 *
 * This plugin is meant to be used with SurveyPad and implements the plugin interface for SurveyPad.
 *
 * The objective of TextPlugin is to be able to open and properly present all possible human redeable text files. In order to do that it provides a number of default lexers/serializers
 * for various languages and file formats. This way the graphical interface of TextPlugin can use these lexers and present the content in appropriate manner.
 *
 * ## Entry Point - `plugin_lexers`
 * As default lexers are not sufficient to present custom Survey file formats properly, the plugin also uses the `plugin_lexers` entry point which is meant to expose the lexers/serializers of different plugins.
 * When TextPlugin graphical project is created, the plugin will search for available lexers exposed with given entry point name and add them to the list of available lexers.
 *
 * ### Example
 * The classes registered under this name should have two methods: `getLexers()` and `getNames()` - first one returning a `QList` of `std::functions` to create lexers and the other a `QList` of `std::strings` with their names.
 * Below is the example interface of a class that can be registered under `plugin_lexers` entrypoint:
 *
 * @code{cpp}
 * #include <functional>
 *
 * #include <QList>
 * #include <QObject>
 *
 * class QsciLexer;
 *
 * class EPplugin_lexers : public QObject
 * {
 *     Q_OBJECT
 *
 * public:
 *     Q_INVOKABLE EPplugin_lexers(QObject *parent = nullptr) : QObject(parent) {}
 *
 *     Q_INVOKABLE QList<QString> getNames();
 *     Q_INVOKABLE QList<std::function<QsciLexer *(QWidget *)>> getLexers();
 * };
 * @endcode
 *
 * ### Entry Point registration
 * In order to register the class one should use the following command:
 *
 * @code{cpp}
 * #include <interface/SPluginLoader.hpp>
 * #include "entrypoints/EPplugin_lexers.hpp"
 *
 * SPluginLoader::getPluginLoader().registerEntryPoint("plugin_lexers", {&EPplugin_lexers::staticMetaObject, owningPluginPointer});
 * @endcode
 */
class TextPlugin : public QObject, public SPluginInterface
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID SPluginInterfaceIID)
	Q_INTERFACES(SPluginInterface)

public:
	TextPlugin(QObject *parent = nullptr);
	virtual ~TextPlugin() override = default;

	virtual const QString &name() const noexcept override { return TextPlugin::_name(); }
	virtual QIcon icon() const override;

	virtual void init() override;
	virtual void terminate() override;

	virtual bool hasConfigInterface() const noexcept override { return true; }
	virtual SConfigWidget *configInterface(QWidget *parent = nullptr) override;
	virtual bool hasGraphicalInterface() const noexcept override { return true; }
	virtual SGraphicalWidget *graphicalInterface(QWidget *parent = nullptr) override;
	virtual bool hasLaunchInterface() const noexcept override { return false; }
	virtual SLaunchObject *launchInterface(QObject * = nullptr) override { return nullptr; }

	virtual QString getExtensions() const noexcept override;
	virtual bool isMonoInstance() const noexcept override { return false; }

// not exposed methods
public:
	static const QString &_name();
};

#endif // TEXTPLUGIN_HPP
