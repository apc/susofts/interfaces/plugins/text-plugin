#include "TextGraphicalWidget.hpp"

#include <QAction>
#include <QBoxLayout>
#include <QDir>
#include <QFileInfo>
#include <QLabel>

#include <ShareablePoints/ShareablePointsList.hpp>
#include <editors/text/ShareablePointsListIOLexer.hpp>
#include <editors/text/TextEditor.hpp>
#include <editors/text/TextSearch.hpp>
#include <interface/SPluginLoader.hpp>

#include "Lexers.hpp"
#include "TextPlugin.hpp"

class TextGraphicalWidget::_TextGraphicalWidget_pimpl
{
public:
	QLabel *titleLabel = nullptr;
	std::function<QsciLexer *(QWidget *)> currentLexerBuilder = nullptr;
};

TextGraphicalWidget::TextGraphicalWidget(SPluginInterface *owner, QWidget *parent) :
	TextEditorScriptWidget(owner, parent), _pimpl(std::make_unique<_TextGraphicalWidget_pimpl>())

{
	enablefileWatcher(true);

	// Setting up the layout
	auto *vlayout = qobject_cast<QVBoxLayout *>(layout());
	if (vlayout)
	{
		QLabel *title = new QLabel(this);
		title->setFrameStyle(QFrame::StyledPanel);
		title->setText("New File");
		title->setTextInteractionFlags(Qt::TextInteractionFlag::TextBrowserInteraction);
		title->setOpenExternalLinks(true);
		_pimpl->titleLabel = title;
		vlayout->insertWidget(0, title);
	}
	installEventFilterAll(this, this);

	// setup editor
	editor()->setMimeTypesSupport(
		{"application/vnd.cern.susoft.surveypad.plugin.text.dummy"}, [this](const QMimeData *mimedata) -> QByteArray { return fromMime(mimedata); },
		[this](const QByteArray &text, QMimeData *mimedata) -> QMimeData * { return toMime(text, mimedata); });

	createActions();
	updateUi(owner->name());
	setWindowTitle("New text file");

	setupScripts();
}

TextGraphicalWidget ::~TextGraphicalWidget() = default;


bool TextGraphicalWidget::_save(const QString &path)
{
	auto result = TextEditorWidget::_save(path);
	if (result)
	{
		setWindowTitle(QFileInfo(path).fileName());
		_pimpl->titleLabel->setText(QString("<a href=\"%1\">%2</a>").arg("file:///" + path).arg(QDir::toNativeSeparators(path)));
	}
	return result;
}

bool TextGraphicalWidget::_open(const QString &path)
{
	auto result = TextEditorWidget::_open(path);
	if (result)
	{
		setWindowTitle(QFileInfo(path).fileName());
		_pimpl->titleLabel->setText(QString("<a href=\"%1\">%2</a>").arg("file:///" + path).arg(QDir::toNativeSeparators(path)));
	}
	return result;
}

QByteArray TextGraphicalWidget::fromMime(const QMimeData *mimedata)
{
	if (!_pimpl->currentLexerBuilder)
		return "";
	auto *io = qobject_cast<ShareablePointsListIOLexer *>(_pimpl->currentLexerBuilder(nullptr));
	if (!io)
		return "";
	io->utf8(editor()->isUtf8());
	return contentFromMime(mimedata, *io);
}

QMimeData *TextGraphicalWidget::toMime(const QByteArray &text, QMimeData *mimedata)
{
	if (!_pimpl->currentLexerBuilder)
		return mimedata;
	auto *io = qobject_cast<ShareablePointsListIOLexer *>(_pimpl->currentLexerBuilder(nullptr));
	if (!io)
		return mimedata;
	io->utf8(editor()->isUtf8());
	return contentToMime(text, mimedata, *io);
}

void TextGraphicalWidget::createActions()
{
	// Add action to the menu that changes editor's lexer
	auto addLexerAction = [this](const QString &name, std::function<QsciLexer *(QWidget *)> lexer) -> void {
		auto *action = new QAction(tr("Set %1 language").arg(name), this);
		connect(action, &QAction::triggered, [lexer, this]() -> void {
			editor()->setLexer(lexer(this));
			_pimpl->currentLexerBuilder = lexer;
		});
		addAction(Menus::plugin, action);
	};

	// Add a separator
	auto addSeparator = [this]() {
		auto *separator = new QAction();
		separator->setSeparator(true);
		addAction(Menus::plugin, separator);
	};

	// Add default (so no lexer at all) and a separator
	addLexerAction("Default", [](QWidget *) -> QsciLexer * { return nullptr; });
	addSeparator();

	// Check for entry point lexers
	auto eplexers = SPluginLoader::getPluginLoader().getEntryPoints("plugin_lexers");
	std::sort(eplexers.begin(), eplexers.end(), [](EntryPoint &p1, EntryPoint &p2) {
		if (p1.plugin && p2.plugin && p1.plugin != p2.plugin)
			return p1.plugin->name() < p2.plugin->name();
		if (p1.plugin && !p2.plugin)
			return false;
		if (!p1.plugin && p2.plugin)
			return true;
		if (p1.meta && p2.meta && p1.meta != p2.meta)
			return p1.meta->className() < p2.meta->className();
		if (p1.meta && !p2.meta)
			return false;
		return true;
	});
	for (const auto &ep : eplexers)
	{
		std::unique_ptr<QObject> qobj(ep.meta->newInstance());
		if (!qobj)
			continue;

		QList<std::function<QsciLexer *(QWidget *)>> lexers;
		QList<QString> names;
		bool callLexer = QMetaObject::invokeMethod(qobj.get(), "getLexers", Q_RETURN_ARG(QList<std::function<QsciLexer *(QWidget *)>>, lexers));
		bool callNames = QMetaObject::invokeMethod(qobj.get(), "getNames", Q_RETURN_ARG(QList<QString>, names));
		if (!callLexer || !callNames || (lexers.size() != names.size()))
			continue;

		for (int i = 0; i < lexers.size(); i++)
			addLexerAction(names[i], lexers[i]);

		// Separator to distinguish SurveyPad lexers from the others
		addSeparator();
	}
	// Basic lexers
	for (auto &[name, lexer] : LEXERMAP)
		addLexerAction(QString::fromStdString(name), lexer);
}
