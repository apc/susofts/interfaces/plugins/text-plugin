#include "TextPlugin.hpp"

#include <Logger.hpp>
#include <QtStreams.hpp>

#include "Version.hpp"
#include "trinity/TextConfig.hpp"
#include "trinity/TextGraphicalWidget.hpp"

TextPlugin::TextPlugin(QObject *parent) : QObject(parent)
{
}

QIcon TextPlugin::icon() const
{
	return QIcon(":/icons/text-icon");
}

void TextPlugin::init()
{
	logDebug() << "Plugin '" << name() << "' loaded";
}

void TextPlugin::terminate()
{
	logDebug() << "Plugin '" << name() << "' unloaded";
}

SConfigWidget *TextPlugin::configInterface(QWidget *parent)
{
	logDebug() << "loading TextPlugin::configInterface (TextConfig)";
	return new TextConfig(this, parent);
}

SGraphicalWidget *TextPlugin::graphicalInterface(QWidget *parent)
{
	logDebug() << "loading TextPlugin::graphicalInterface (TextGraphicalWidget)";
	return new TextGraphicalWidget(this, parent);
}

QString TextPlugin::getExtensions() const noexcept
{
	return tr("Any files (*);;Text (*.txt)");
}

const QString &TextPlugin::_name()
{
	static const QString _sname = QString::fromStdString(getPluginName());
	return _sname;
}
