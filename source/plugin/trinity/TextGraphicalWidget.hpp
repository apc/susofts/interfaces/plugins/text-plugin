/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef TEXTGRAPHICALWIDGET_HPP
#define TEXTGRAPHICALWIDGET_HPP

#include <editors/text/TextEditorWidget.hpp>
#include <editors/script/EditorScriptWidgets.hpp>

/**
 * Graphical interface for Text Plugin.
 *
 * This graphical interface is meant to handle any human redeable text files that SurveyPad may want to open.
 *
 * It is composed of one tab only that inherits from TextEditorWidget, and therefore, provides its basic benefits and functionality related to text manipulation.
 * Moreover, it provides a number of different QScintilla lexers that allows styling of the contents with different colouring.
 */
class TextGraphicalWidget : public TextEditorScriptWidget
{
	Q_OBJECT

public:
	TextGraphicalWidget(SPluginInterface *owner, QWidget *parent = nullptr);
	virtual ~TextGraphicalWidget() override;

protected:
	// SGraphicalWidget
	virtual bool _save(const QString &path) override;
	virtual bool _open(const QString &path) override;

private:
	// TextEditorWidget
	QByteArray fromMime(const QMimeData *mimedata);
	QMimeData *toMime(const QByteArray &text, QMimeData *mimedata);

	/** Creates the actions for all the available lexers. */
	void createActions();

private:
	class _TextGraphicalWidget_pimpl;
	std::unique_ptr<_TextGraphicalWidget_pimpl> _pimpl;
};

#endif // TEXTGRAPHICALWIDGET_HPP
