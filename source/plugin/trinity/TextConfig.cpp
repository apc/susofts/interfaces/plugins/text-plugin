#include "TextConfig.hpp"
#include "ui_TextConfig.h"

#include <QSettings>

#include "TextPlugin.hpp"
#include "Version.hpp"

bool TextConfigObject::operator==(const SConfigObject &o) const noexcept
{
	if (!SConfigObject::operator==(o))
		return false;
	return true;
}

void TextConfigObject::write() const
{
	QSettings settings;
	settings.beginGroup(TextPlugin::_name());

	settings.endGroup();
}

void TextConfigObject::read()
{
	QSettings settings;
	settings.beginGroup(TextPlugin::_name());

	settings.endGroup();
}

TextConfig::TextConfig(SPluginInterface *owner, QWidget *parent) : SConfigWidget(owner, parent), ui(std::make_unique<Ui::TextConfig>())
{
	ui->setupUi(this);
	ui->lblVersion->setText(ui->lblVersion->text().arg(QString::fromStdString(getVersion())));
}

TextConfig::~TextConfig() = default;

SConfigObject *TextConfig::config() const
{
	auto *tmp = new TextConfigObject();
	return tmp;
}

void TextConfig::setConfig(const SConfigObject *conf)
{
	const auto *config = dynamic_cast<const TextConfigObject *>(conf);
	if (config)
	{
	}
}

void TextConfig::reset()
{
	TextConfigObject config;
	config.read();
	setConfig(&config);
}

void TextConfig::restoreDefaults()
{
	TextConfigObject config;
	setConfig(&config);
}
