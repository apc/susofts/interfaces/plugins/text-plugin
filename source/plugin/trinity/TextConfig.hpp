/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef TEXTCONFIG_HPP
#define TEXTCONFIG_HPP

#include <interface/SConfigInterface.hpp>

namespace Ui
{
class TextConfig;
}

struct TextConfigObject : public SConfigObject
{
	virtual ~TextConfigObject() override = default;

	bool operator==(const SConfigObject &o) const noexcept override;

	virtual void write() const override;
	virtual void read() override;
};

class TextConfig : public SConfigWidget
{
	Q_OBJECT

public:
	TextConfig(SPluginInterface *owner, QWidget *parent = nullptr);
	virtual ~TextConfig();

	// SConfigInterface
	virtual SConfigObject *config() const override;
	virtual void setConfig(const SConfigObject *conf) override;
	virtual void reset() override;
	virtual void restoreDefaults() override;

private:
	std::unique_ptr<Ui::TextConfig> ui;
};

#endif // TEXTCONFIG_HPP
